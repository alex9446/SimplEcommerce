import logging
import sqlite3
import sys
from typing import Any, List

from tabulate import tabulate

from interrogazioni_db import (delete_cliente, delete_fornitore,
                               insert_cliente, insert_fornitore,
                               select_cliente, select_fornitore,
                               update_cliente, update_fornitore)

LOGGER = logging.getLogger(__name__)


def convalida_input(etichetta: str,
                    obbligatorio=False,
                    predefinito='',
                    valori_non_accettati=[]) -> str:
    # Formattazione dell'etichetta diversificata
    # se l'immissione di testo è obbligatoria o vi è un valore predefinito
    if predefinito:
        etichetta = '{} [{}]: '.format(etichetta, predefinito)
    elif obbligatorio:
        etichetta = '* {}: '.format(etichetta)
    else:
        etichetta = '{}: '.format(etichetta)
    input_inserito = ''
    # Il ciclo si interromperà quando viene inserito un testo
    # e il valore inserito non fa parte di quelli esclusi,
    # viene anche interroto se non è obbligatorio o vi è un valore predefinito.
    while not input_inserito:
        input_inserito = input(etichetta)
        if input_inserito:
            if input_inserito in valori_non_accettati:
                print('Valore non accettato')
                input_inserito = ''
        else:
            if predefinito:
                input_inserito = predefinito
            elif obbligatorio:
                print('Campo obbligatorio!')
            else:
                break
    return input_inserito


def vista_tabella(lista_righe, mostra_numero_riga=False,
                  *args, **kwargs) -> None:
    if mostra_numero_riga:
        # Creo una lista equivalente al nuomero di righe, partendo da 1
        mostra_numero_riga = range(1, len(lista_righe)+1)
    # Impostazione predefinita per la formattazione delle tabelle
    print(tabulate(lista_righe, showindex=mostra_numero_riga,
                   tablefmt='fancy_grid', *args, **kwargs))


class SimplEcommerce:
    def __init__(self):
        # Info sulle versioni delle librerie
        LOGGER.info('Python version: %s', sys.version.split(' ')[0])
        LOGGER.info('Sqlite3 version: %s', sqlite3.version)
        LOGGER.info('SQLite version: %s', sqlite3.sqlite_version)

    def connetti_db(self) -> None:
        # Connessione al database sqlite
        self.connessione_db = sqlite3.connect('db.sqlite')
        self.cursore_db = self.connessione_db.cursor()

    def inizializzazione_db(self) -> None:
        # Creazione delle tabbelle se non esistenti
        with open('creazione_tabelle.sql') as f:
            self.cursore_db.executescript(f.read())

    def disconnetti_db(self) -> None:
        # Disconnessione dal database sqlite
        self.cursore_db.close()
        self.connessione_db.close()

    def lista_clienti(self, solo_chiavi=False) -> List[Any]:
        self.cursore_db.execute(select_cliente)
        # Ritorna la lista dei clienti presenti nel database
        clienti = self.cursore_db.fetchall()
        if solo_chiavi:
            return [cliente[0] for cliente in clienti]
        else:
            return clienti

    def inserisci_cliente(self, codice_fiscale, nome, cognome,
                          via, cap, citta, nazione) -> None:
        # Inserimento di un nuovo cliente
        self.cursore_db.execute(
            insert_cliente,
            (codice_fiscale, nome, cognome, via, cap, citta, nazione)
        )
        self.connessione_db.commit()

    def modifica_cliente(self, codice_fiscale, nome, cognome,
                         via, cap, citta, nazione, chiave_cliente) -> None:
        # Modifica del cliente con i nuovi valori
        self.cursore_db.execute(update_cliente,
                                (codice_fiscale, nome, cognome,
                                 via, cap, citta, nazione, chiave_cliente))
        self.connessione_db.commit()

    def elimina_cliente(self, chiave_cliente) -> None:
        # Eliminazione del cliente
        self.cursore_db.execute(delete_cliente, (chiave_cliente,))
        self.connessione_db.commit()

    def lista_fornitori(self, solo_chiavi=False) -> List[Any]:
        self.cursore_db.execute(select_fornitore)
        # Ritorna la lista dei fornitori presenti nel database
        fornitori = self.cursore_db.fetchall()
        if solo_chiavi:
            return [fornitore[0] for fornitore in fornitori]
        else:
            return fornitori

    def inserisci_fornitore(self, partita_iva,
                            ragione_sociale, nazione) -> None:
        # Inserimento di un nuovo fornitore
        self.cursore_db.execute(insert_fornitore,
                                (partita_iva, ragione_sociale, nazione))
        self.connessione_db.commit()

    def modifica_fornitore(self, partita_iva, ragione_sociale,
                           nazione, chiave_fornitore) -> None:
        # Modifica del fornitore con i nuovi valori
        self.cursore_db.execute(
            update_fornitore,
            (partita_iva, ragione_sociale, nazione, chiave_fornitore)
        )
        self.connessione_db.commit()

    def elimina_fornitore(self, chiave_fornitore) -> None:
        # Eliminazione del fornitore
        self.cursore_db.execute(
            delete_fornitore,
            (chiave_fornitore,)
        )
        self.connessione_db.commit()


class SimplEcommerceCLI(SimplEcommerce):
    def __init__(self):
        super().__init__()
        print()
        self.connetti_db()
        self.inizializzazione_db()
        self.menu()
        self.disconnetti_db()

    def menu(self) -> None:
        while True:
            # Stampa delle opzioni disponibili, ordinate per numero
            print('1 - Questo menù')
            print('--- Gestione clienti')
            print('2 - Lista clienti')
            print('3 - Inserisci cliente')
            print('4 - Modifica cliente')
            print('5 - Elimina cliente')
            print('--- Gestione fornitori')
            print('6 - Lista fornitori')
            print('7 - Inserisci fornitore')
            print('8 - Modifica fornitore')
            print('9 - Elimina fornitore')
            print('q - Esci')
            voce_selezionata = input('Scegli un opzione [1-9]: ')
            try:
                voce_selezionata = int(voce_selezionata)
            except ValueError:
                # Esce se viene inserita la lettera q
                if voce_selezionata == 'q':
                    break
            if voce_selezionata == 1:
                continue
            elif voce_selezionata == 2:
                self.lista_clienti()
            elif voce_selezionata == 3:
                self.inserisci_cliente()
            elif voce_selezionata == 4:
                self.modifica_cliente()
            elif voce_selezionata == 5:
                self.elimina_cliente()
            elif voce_selezionata == 6:
                self.lista_fornitori()
            elif voce_selezionata == 7:
                self.inserisci_fornitore()
            elif voce_selezionata == 8:
                self.modifica_fornitore()
            elif voce_selezionata == 9:
                self.elimina_fornitore()
            else:
                print('\nVoce non presente\n')

    def lista_clienti(self) -> None:
        # Stampa la lista dei clienti presenti nel database
        vista_tabella(
            super().lista_clienti(),
            headers=['Codice fiscale', 'Nome', 'Cognome',
                     'Via', 'CAP', 'Citta', 'Nazione']
        )

    def inserisci_cliente(self) -> None:
        # Recupera le chiavi primarie già usate nella tabella dei clienti
        print('\nCampi con * obbligatori')
        codice_fiscale = convalida_input(
            'Codice fiscale', obbligatorio=True,
            valori_non_accettati=super().lista_clienti(solo_chiavi=True)
        )
        nome = convalida_input('Nome', obbligatorio=True)
        cognome = convalida_input('Cognome', obbligatorio=True)
        via = convalida_input('Via')
        cap = convalida_input('CAP')
        citta = convalida_input('Città')
        nazione = convalida_input('Nazione')
        # Inserimento di un nuovo cliente con i valori richiesti
        super().inserisci_cliente(codice_fiscale, nome, cognome,
                                  via, cap, citta, nazione)
        print('\nCliente inserito\n')

    def selezione_cliente(self) -> List[Any]:
        clienti = super().lista_clienti()
        if clienti:
            # Stampa la lista dei clienti presenti nel database,
            # con l'aggiunta di un numero associato ad ogni cliente
            print('\nLista clienti:')
            vista_tabella(clienti, mostra_numero_riga=True,
                          headers=['N°', 'Codice fiscale', 'Nome', 'Cognome',
                                   'Via', 'CAP', 'Citta', 'Nazione'])
            try:
                # Selezione cliente, con suggerimento valori minimi e massimi
                numero_cliente = int(input(
                    'Cliente da modificare/eliminare [1-{}]: '
                    .format(len(clienti)))
                )
            except ValueError:
                print('\nValore inserito non numerico\n')
            else:
                # Verifica se il numero cliente è un valore presente
                if (numero_cliente > 0) and (numero_cliente <= len(clienti)):
                    # Ritorna il cliente selezionato dall'utente
                    return clienti[numero_cliente-1]
                print('\nNumero cliente inesistente\n')
        else:
            print('\nNessun cliente presente\n')
        return []

    def modifica_cliente(self) -> None:
        cliente_scelto = self.selezione_cliente()
        if cliente_scelto:
            # Richiesta dei nuovi valori, se non viene inserito testo
            # sarà usato quello già presente per il cliente
            chiavi_gia_usate = super().lista_clienti(solo_chiavi=True)
            chiavi_gia_usate.remove(cliente_scelto[0])
            codice_fiscale = convalida_input(
                'Codice fiscale',
                predefinito=cliente_scelto[0],
                valori_non_accettati=chiavi_gia_usate
            )
            nome = convalida_input('Nome', predefinito=cliente_scelto[1])
            cognome = convalida_input('Cognome', predefinito=cliente_scelto[2])
            via = convalida_input('Via', predefinito=cliente_scelto[3])
            cap = convalida_input('CAP', predefinito=cliente_scelto[4])
            citta = convalida_input('Città', predefinito=cliente_scelto[5])
            nazione = convalida_input('Nazione', predefinito=cliente_scelto[6])
            # Modifica del cliente con i nuovi valori
            super().modifica_cliente(codice_fiscale, nome, cognome, via, cap,
                                     citta, nazione, cliente_scelto[0])
            print('\nCliente modificato\n')

    def elimina_cliente(self) -> None:
        cliente_scelto = self.selezione_cliente()
        if cliente_scelto:
            # Eliminazione del cliente con conferma
            if input('Vuoi davvero eliminare {} [s/N]: '
                     .format(cliente_scelto[0])).lower() in ['s', 'y']:
                super().elimina_cliente(cliente_scelto[0])
                print('\nCliente eliminato\n')

    def lista_fornitori(self) -> None:
        # Stampa la lista dei fornitori presenti nel database
        vista_tabella(
            super().lista_fornitori(),
            headers=['Partita IVA', 'Ragione sociale', 'Nazione']
        )

    def inserisci_fornitore(self) -> None:
        # Recupera le chiavi primarie già usate nella tabella dei fornitori
        print('\nCampi con * obbligatori')
        partita_iva = convalida_input(
            'Partita IVA', obbligatorio=True,
            valori_non_accettati=super().lista_fornitori(solo_chiavi=True)
        )
        ragione_sociale = convalida_input('Ragione sociale', obbligatorio=True)
        nazione = convalida_input('Nazione')
        # Inserimento di un nuovo fornitore con i valori richiesti
        super().inserisci_fornitore(partita_iva, ragione_sociale, nazione)
        print('\nFornitore inserito\n')

    def seleziona_fornitore(self) -> List[Any]:
        fornitori = super().lista_fornitori()
        if fornitori:
            # Stampa la lista dei fornitori presenti nel database,
            # con l'aggiunta di un numero associato ad ogni fornitore
            print('\nLista fornitori:')
            vista_tabella(fornitori, mostra_numero_riga=True,
                          headers=['N°', 'Partita IVA',
                                   'Ragione sociale', 'Nazione'])
            try:
                # Selezione fornitore, con suggerimento valori minimi e massimi
                numero_fornitore = int(input(
                    'Fornitore da modificare/eliminare [1-{}]: '
                    .format(len(fornitori)))
                )
            except ValueError:
                print('\nValore inserito non numerico\n')
            else:
                # Verifica se il numero fornitore è un valore presente
                if (numero_fornitore > 0) and \
                   (numero_fornitore <= len(fornitori)):
                    # Ritorna il fornitore selezionato dall'utente
                    return fornitori[numero_fornitore-1]
                print('\nNumero fornitore inesistente\n')
        else:
            print('\nNessun fornitore presente\n')
        return []

    def modifica_fornitore(self) -> None:
        fornitore_scelto = self.seleziona_fornitore()
        if fornitore_scelto:
            # Richiesta dei nuovi valori, se non viene inserito testo
            # sarà usato quello già presente per il fornitore
            chiavi_gia_usate = super().lista_fornitori(solo_chiavi=True)
            chiavi_gia_usate.remove(fornitore_scelto[0])
            partita_iva = convalida_input(
                'Partita IVA',
                predefinito=fornitore_scelto[0],
                valori_non_accettati=chiavi_gia_usate
            )
            ragione_sociale = convalida_input('Ragione sociale',
                                              predefinito=fornitore_scelto[1])
            nazione = convalida_input('Nazione',
                                      predefinito=fornitore_scelto[2])
            # Modifica del fornitore con i nuovi valori
            super().modifica_fornitore(partita_iva, ragione_sociale,
                                       nazione, fornitore_scelto[0])
            print('\nFornitore modificato\n')

    def elimina_fornitore(self) -> None:
        fornitore_scelto = self.seleziona_fornitore()
        if fornitore_scelto:
            # Eliminazione del fornitore con conferma
            if input('Vuoi davvero eliminare {} [s/N]: '
                     .format(fornitore_scelto[0])).lower() in ['s', 'y']:
                super().elimina_fornitore(fornitore_scelto[0])
                print('\nFornitore eliminato\n')


if __name__ == '__main__':
    # Se viene passato l'aromento --verbose, il logging viene impostato
    # sul livello di debug, altrimenti il livello predefinito sarà info
    livello_log = logging.DEBUG if '--verbose' in sys.argv else logging.INFO
    logging.basicConfig(
        level=livello_log,
        format='%(asctime)s %(levelname)s (%(threadName)s) '
        '[%(name)s] %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S'
    )
    SimplEcommerceCLI()
