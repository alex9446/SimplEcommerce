select_cliente = '''
SELECT CodiceFiscale, Nome, Cognome, Via, CAP, Citta, Nazione
FROM Cliente
'''

insert_cliente = '''
INSERT INTO Cliente
(CodiceFiscale, Nome, Cognome, Via, CAP, Citta, Nazione)
VALUES (?, ?, ?, ?, ?, ?, ?)
'''

update_cliente = '''
UPDATE Cliente
SET CodiceFiscale=?, Nome=?, Cognome=?, Via=?, CAP=?, Citta=?, Nazione=?
WHERE CodiceFiscale = ?
'''

delete_cliente = '''
DELETE FROM Cliente
WHERE CodiceFiscale = ?
'''

select_fornitore = '''
SELECT PartitaIva, RagioneSociale, Nazione
FROM Fornitore
'''

insert_fornitore = '''
INSERT INTO Fornitore
(PartitaIva, RagioneSociale, Nazione)
VALUES (?, ?, ?)
'''

update_fornitore = '''
UPDATE Fornitore
SET PartitaIva=?, RagioneSociale=?, Nazione=?
WHERE PartitaIva = ?
'''

delete_fornitore = '''
DELETE FROM Fornitore
WHERE PartitaIva = ?
'''
